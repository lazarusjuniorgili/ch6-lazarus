// const fs = require("fs");
const express = require("express");
const app = express();
const { user_game, user_game_biodata, user_game_history } = require("./models");
const PORT = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// -----------------------------------------------------------------------------------------

app.post("/user", (reg, res) => {
  const { user_name, password, email, result, score, nama_lengkap, alamat } = reg.body;

  user_game
    .create({
      user_name,
      password,
      email,
    })
    .then((user) => {
      user_game_history
        .create({
          result,
          score,
          user_id: user.id,
        })
        .then((user) => {
          console.log({ message: "Create Success", data: user });
        });

      user_game_biodata
        .create({
          nama_lengkap,
          alamat,
          user_id: user.id,
        })
        .then((user) => {
          console.log({ message: "Create Success", data: user });
        });

      res.json({ message: "Create Success", data: user });
    });
});

// ---------------------------------------------------------------------------------------

// app.post("/usergames", (reg, res) => {
//   const { user_name, password, email } = reg.body;

//   user_game
//     .create({
//       user_name: user_name,
//       password: password,
//       email: email,
//     })
//     .then((game) => {
//       res.json({ message: "Create Success", data: game });
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// });

// ---------------------------------------------------------------------------------------

app.get("/userall", (reg, res) => {
  user_game.findAll({ include: ["history", "user"] }).then((game) => {
    res.json({ message: "Fetch All Success", data: game });
  });
});

// ----------------------------------------------------------------------------------------

app.get("/usergames/:id", (reg, res) => {
  const { id } = reg.params;

  user_game.findOne({ where: { id: id } }).then((game) => {
    res.json({ message: "Fetch Success", data: game });
  });
});

// ----------------------------------------------------------------------------------------

app.put("/user/:id", (reg, res) => {
  const { id } = reg.params;
  const { user_name, password, email, result, score, nama_lengkap, alamat } = reg.body;
  user_game
    .update(
      {
        user_name: user_name,
        password: password,
        email: email,
        result: result,
        score: score,
        nama_lengkap: nama_lengkap,
        alamat: alamat,
      },
      { where: { id: id } }
    )
    .then(() => {
      res.json({ message: "Update Success" });
    })
    .catch((err) => {
      res.json({ message: "Gagal Mengupdate User" });
    });
});

// -----------------------------------------------------------------------------------------

// app.put("/user/:id", (reg, res) => {
//   const { id } = reg.params;
//   const { user_name, password, email, result, score, nama_lengkap, alamat } = reg.body;

//   user_game
//     .update(
//       {
//         user_name,
//         password,
//         email,
//       },
//       { where: { id: id } }
//     )
//     .then((user) => {
//       user_game_history
//         .update({
//           result,
//           score,
//           user_id: user.id,
//         })
//         .then((user) => {
//           console.log({ message: "Update Success", data: user });
//         });

//       user_game_biodata
//         .update({
//           nama_lengkap,
//           alamat,
//           user_id: user.id,
//         })
//         .then((user) => {
//           console.log({ message: "Update Success", data: user });
//         });

//       res.json({ message: "Update Success", data: user });
//     });
// });

// -----------------------------------------------------------------------------------------

app.delete("/usergames/:id", (reg, res) => {
  const { id } = reg.params;
  user_game
    .destroy({ where: { id: id } })
    .then(() => {
      res.json({ message: "Delete Success" });
    })
    .catch((err) => {
      res.json({ message: "Gagal Mengahapus Data" });
    });
});

// -----------------------------------------------------------------------------------------

app.listen(PORT, () => console.log(`Listen at http://localhost:${PORT}`));
